use rand::Rng;
use std::io;

fn main() {
    println!("Welcome to rock-paper-scissors");
    println!("(1) is for rock, (2) is for paper, (3) is for scissor");
    println!("Your Turn ->");
    let mut turn = String::new();
    io::stdin()
        .read_line(&mut turn)
        .expect("Please Enter a number!");
    let computer = rand::thread_rng().gen_range(1..4);
    let turn: i32 = turn.trim().parse().expect("Please Enter a number!, or if you entered a number bigger than 2147483647, or smaller than -2147483648, please enter a number that is between this range!");
    if turn > 3 {
        println!("You may have entered a number greater than 3, please enter a number smaller than or equal to 3");
    } else {
        println!("Nothing happened!");
    }
    println!("Computer's turn ->");
    println!("{}", computer);
    if (turn == 1) && (computer == 2) {
        println!("You lose");
    } else if (turn == 2) && (computer == 3) {
        println!("You lose");
    } else if (turn == 3) && (computer == 1) {
        println!("You lose");
    } else if (turn == 2) && (computer == 2) {
        println!("Tie");
    } else if (turn == 1) && (computer == 1) {
        println!("Tie");
    } else if (turn == 3) && (computer == 3) {
        println!("Tie");
    } else if (turn == 2) && (computer == 1) {
        println!("You won");
    } else if (turn == 3) && (computer == 2) {
        println!("You won");
    } else if (turn == 1) && (computer == 3) {
        println!("You won");
    } else {
        println!("Probably an error occured!");
    }
}
