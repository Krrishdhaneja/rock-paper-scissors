# Rock-Paper-Scissors

A classic game made in Rust😉.

## Installation:

- This program requires Rust(and it's other components) installed in your system, you can download rust from [here](https://www.rust-lang.org/tools/install). Or if you are running a Linux distro or macOS run the following command:
`curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`

- If the installation of Rust is done then clone this repository and build the program from source:

    <code>git clone https://gitlab.com/Krrishdhaneja/rock-paper-scissors
    cd rock-paper-scissors
    cargo build</code>

(or run `cargo run` to directly run this program).

A new folder named `target` will appear and the built binary can be found in `target/debug` with the name of `rock-paper-scissors` and you can run it by opening a terminal and running the command `./rock-paper-scissors`

- A built binary can be found [here](https://gitlab.com/Krrishdhaneja/rock-paper-scissors/-/releases) for Linux 64-bit, to run the program from the binary, open up a terminal in the directory in which you downloaded the file then type the command `chmod +x ./rock-paper-scissors` to make it executable and then run the command `./rock-paper-scissors` in order to run it.

## Reporting Issue

If you found any vulnerability or if something is not working just click [here](https://gitlab.com/Krrishdhaneja/rock-paper-scissors/-/issues) and open a new issue.